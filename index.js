console.log('Hello World')

// Best practice in naming variables
let firstName = 'John'
console.log("First Name: " + firstName)
let lastName = 'Smith'
console.log("Last Name: " + lastName)
let age = 30
console.log('Age: ' + age)
let hobbies = ["Basketball", "Running", "Watching"];
console.log("Hobbies: ");
console.log(hobbies);
const workAddress  = { 
	
	houseNumber: 27,
	street: 'San Antonio',
	city: 'Paranaque',
	state: 'Metro Manila',
	
};
console.log("Work Address: ");
console.log(workAddress);



// Debugging Practice

let fullName = "Steve Rogers";
console.log("My full name is: " + fullName);

let currentAge = 40;
console.log("My current age is: " + currentAge);

let friends = ["Tony","Bruce","Thor","Natasha","Clint","Nick"];
console.log("My Friends are: ")
console.log(friends);

	let profile = {

		username: "captain_america",
		fullName: "Steve Rogers",
		age: 40,
		isActive: false,

	}
	console.log("My Full Profile: " + profile);
	console.log(profile);

	let bestFriend = "Bucky Barnes";
	console.log("My bestfriend is: " + bestFriend);

	const lastLocation = "Arctic Ocean";
	console.log("I was found frozen in: " + lastLocation);
